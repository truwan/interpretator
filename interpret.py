
# Трубецков Иван 4103 группа интерпретатор подмножества языка Pascal 
# обработка операций с выражениями и строками, передачей управления, встроенными функциями
# обработка ошибок несоответствия типов при выполнении арифметических операций и встроенных функций


import math

keyword_file = 'key_table.txt'
names_file = 'name_table.txt'
sequence_file = 'pol.txt'

## Поиск в таблице имен и таблице ключевых слов
def FindInTables(tofind):
    print(tofind)
    num = int(tofind)
    print('num =',num)
    if num < 0:
        print('less')
        tablename = keyword_file
        t = open(tablename,'r')
        for line in t:
            key_line = line.split()
            key = int(key_line[1])
            if key == num:
                t.close()
                return key_line
    else:
        tablename = names_file
        t = open(tablename,'r')
        for line in t:
            key_line = line.split('%@&')
            i = len(key_line) - 1
            key_line[i] = key_line[i][:-1:]
            print(key_line)
            key = int(key_line[1])
            if key == num:
                t.close()
                return key_line
    t.close()
    return []

## Изменение значения в таблице имен
def change_value(addr,value,val_type):
    addr = int(addr)
    t = open(names_file,'r')
    lines = t.readlines()
    t.close()
    t = open(names_file,'r')
    i = 0
    for line in t:
        key_line = line.split('%@&')
        key_line[4] = key_line[4][:-1:]
        key = int(key_line[1])
        if key == addr:
            t.close()
            value_line = key_line.copy()
            break;
        i+=1
    if len(value_line) == 5:
        value_line[4] = str(value)
    else:
        value_line.append(str(value))
        
    value_line[3] = val_type
    
    lines[i] = '%@&'.join(value_line) + '\n'
    new = open(names_file,'w')
    new.writelines(lines)
    new.close()

## Извлечение из стека
def stack_pop(stack):
    a = stack.pop()
    if(a[0] == 'ptr'):
        a_line = FindInTables(a[1])
        if not a_line:
            print('error pop: no name')
            return []
        if(a_line[2] == 'const'):
            if a_line[3] == 'int':
                a = int(a_line[4])
            if a_line[3] == 'float':
                a = float(a_line[4])
            if a_line[3] == 'str':
                a = a_line[4]
            if a_line[3] == 'label':
                a = a_line[4]
            return ['val',a]
        else:
            #переменная
            a = a_line[1]
            return ['var',a]
    elif a[0] == 'var':
        return ['var',a[1]]
    else:
        #значение
        return ['val',a[1]]

## Извлечение 2 аргументов из стека
def pop2(stack):
    a = stack_pop(stack)
    if not a:
        return []
    if a[0] == 'var':
        r1 = FindInTables(a[1])
        if not r1:
            print('error: no name')
            return []
            
        if len(r1)<5:
            print('error: no value')
            return []
        else:
            if r1[3] == 'int':
                a[1] = float(r1[4]) #всегда считаю float
            elif r1[3] == 'float':
                a[1] = float(r1[4]) #: #str
            else:
                a[1] = r1[4]
    
    b = stack_pop(stack) 
    if not b:
        return []
    if b[0] == 'var':
        r2 = FindInTables(b[1])
        if not r2:
            print('error: no name')
            return []
            
        if len(r2)<5:
            print('error: no value')
            return []
        else:
            if r2[3] == 'int':
                b[1] = float(r2[4]) #всегда считаю float
            elif r2[3] == 'float':
                b[1] = float(r2[4]) 
            elif r2[3] == 'label':
                b[1] = int(r2[4])
            else:
                b[1] = r2[4]
        
    if type(a[1])==int:
        a[1] = float(a[1])
    if type(b[1])==int:
        b[1] = float(b[1])
        
    return [b[1],a[1]]
       
## Извлечение одного аргумента из стека
def pop1(stack):
    a = stack_pop(stack)
    if not a:
        return []
    if a[0] == 'var':
        r1 = FindInTables(a[1])
        if not r1:
            print('error: no name')
            return []
            
        if len(r1)<5:
            print('error: no value')
            return []
        else:
            if r1[3] == 'int':
                a[1] = float(r1[4]) #всегда считаю float
            elif r1[3] == 'float':
                a[1] = float(r1[4]) 
            elif r1[3] == 'label':
                a[1] = int(r1[4])
            else:
                #str
                a[1] = r1[4]
    return [a[1]]

## Определение операторов
def operator(stack,op):
    global pol_form_position
    
    if op == '+':
        operands = pop2(stack)
        if not operands:
            print('+: no operands')
            return 0
        if type(operands[0]) == type(operands[1]):
            stack.append(['val',operands[0]+operands[1]]) 
            return 1 # выполнено
        else:
            print('+: wrong types')
            return 0
        
    if op == '-': # binary
        operands = pop2(stack)
        if not operands:
            print('-: no operands')
            return 0
        if type(operands[0]) == str:
            print('-: #1 wrong types')
            return 0
        
        if type(operands[0]) == type(operands[1]):
            stack.append(['val',operands[0]-operands[1]]) 
            return 1 # выполнено
        else:
            print('-: #2 wrong types')
            return 0
	if op == '~': #unary
        operands = pop1(stack)
        if not operands:
            print('~: no operands')
            return 0
        if type(operands[0]) == str:
            print('~: wrong types')
            return 0
        stack.append(['val',(-1)*operands[0]])
        
        return 1
        
    if op == '*':
        operands = pop2(stack)
        if not operands:
            print('*: no operands')
            return 0
        if type(operands[0]) == str:
            print('*: wrong types')
            return 0
        if type(operands[0]) == type(operands[1]):
            stack.append(['val',operands[0]*operands[1]]) 
            return 1 # выполнено
        else:
            print('*: wrong types')
            return 0
        
    if op == '/':
        operands = pop2(stack)
        if not operands:
            print('/: no operands')
            return 0
        if type(operands[0]) == str:
            print('/: wrong types')
            return 0
        if type(operands[0]) == type(operands[1]):
            stack.append(['val',operands[0]/operands[1]]) 
            return 1 # выполнено
        else:
            print('/: wrong types')
            return 0
        
    if op == ':=':
        value = stack_pop(stack)
        variable = stack_pop(stack) 
        if variable[0] != 'var':
            print('error: not variable')
            return 0
        
        if value[0] == 'var':
            res = FindInTables(value[1])
            
            if not res:
                print('error: no name')
                return 0
            if len(res)<5:
                print('error: no value')
                return 0
            value_type = res[3]
            if value_type == 'name':
                print('error: type = name')
                return 0
            if value_type == 'int':
                value_type = 'float'
                value[1] = float(res[4])         
            elif value_type == 'float':
                value_type = 'float'
                value[1] = float(res[4]) 
            else:
                #str
                value[1] = res[4]
            
        if type(value[1]) == int:
            value_type = 'float'
        if type(value[1]) == float:
            value_type = 'float'
        if type(value[1]) == str:
            value_type = 'str'
        change_value(variable[1],value[1],value_type)
        return 1
    
    if op == 'writeln':
        operands = pop1(stack)
        if not operands:
            print('writeln: no operands')
            return 0
        print(operands[0])
        return 1
    
    if op == 'sin':
        operands = pop1(stack)
        if not operands:
            print('sin: no operands')
            return 0
        if type(operands[0]) == float:
            stack.append(['val',math.sin(operands[0])])
            return 1
        else:
            return 0
        
    
    if op == 'abs':
        operands = pop1(stack)
        if not operands:
            print('abs: no operands')
            return 0
        if type(operands[0]) == float:
            stack.append(['val',abs(operands[0])])
            return 1
        else:
            return 0

    if op == 'goto':
        operands = pop1(stack)
        if not operands:
            print('$goto: no operands')
            return 0
        pol_form_position = int(operands[0])
        return 1
    
    if op == '$BR':
        label = stack.pop()
        label = int(label[1])
        pol_form_position = label
        return 1        
        
    if op == '$BRZ':
        label = stack.pop()
        label = int(label[1])
        sign = stack.pop()
        rside = pop1(stack)
        lside = pop1(stack)
        
        if type(rside) == str:
            print('$BRZ: wrong types')
            return 0
        
        if type(lside) == str:
            print('$BRZ: wrong types')
            return 0
        
        if sign == '>':
            if lside > rside:
                return 1
        
        if sign == '<':
            if lside < rside:
                return 1
        
        if sign == '=':
            if lside == rside:
                return 1
        pol_form_position = label
        return 1
    
    if op == '>':
        stack.append('>')
        return 1
    
    if op == '<':
        stack.append('<')
        return 1
    
    if op == '=':
        stack.append('=')
        return 1

f = open(sequence_file,'r')
prog = f.read().split()
f.close()


stack = []
# 0 - адрес
# 1 - число
pol_form_position = 0
while pol_form_position < (len(prog)):
    if int(prog[pol_form_position]) > 0:
        stack.append(['ptr',prog[pol_form_position]])
    else:
        op = FindInTables(prog[pol_form_position])[0]
        res = operator(stack,op)
        if res == 0:
            break
    pol_form_position+=1    
    
